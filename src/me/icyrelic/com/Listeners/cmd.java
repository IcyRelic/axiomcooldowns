package me.icyrelic.com.Listeners;

import java.util.List;
import java.util.UUID;

import me.icyrelic.com.AxiomCooldowns;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class cmd implements Listener {
	
	AxiomCooldowns plugin;
	public cmd(AxiomCooldowns instance){
		plugin = instance;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onCommand(PlayerCommandPreprocessEvent e){
		String[] message = e.getMessage().split(" ");
		String cmd = message[0].replace("/", "");
		Player p = e.getPlayer();
		UUID uid = p.getUniqueId();
		
		List<String> cooldownCMDs = plugin.getConfig().getStringList("commands");
		
		for(String cdCmd : cooldownCMDs){
			String[] split = cdCmd.split(" %");
			String command = split[0];
			int cooldown = Integer.parseInt(split[1]);
			
			String perm = command+cooldown;
			
			if(cmd.equalsIgnoreCase(command) && !p.hasPermission("axiomcooldowns."+perm)){
				if(plugin.cooldowns.containsKey(uid)) {
		            long secondsLeft = ((plugin.cooldowns.get(uid)/1000)+cooldown) - (System.currentTimeMillis()/1000);
		            if(secondsLeft>0) {
		                // Still cooling down
		                p.sendMessage(plugin.prefix + ChatColor.RED + "You can't use that command for "+ secondsLeft +" seconds!");
		                e.setCancelled(true);
		            }else{
			        	//Cooldown Expired
						plugin.cooldowns.put(uid, System.currentTimeMillis());	
			        }
		        }else{
		        	//No cooldown found
					plugin.cooldowns.put(uid, System.currentTimeMillis());	
		        }
				
				
			}
		}
		
		
	}

}
