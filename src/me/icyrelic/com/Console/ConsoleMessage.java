package me.icyrelic.com.Console;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;

public class ConsoleMessage {
	
	private String prefix = "";
	
	public ConsoleMessage(String pluginPrefix){
		prefix = pluginPrefix;
	}
	
	public void sendMessage(String msg){
		ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
		
		console.sendMessage(prefix + msg);
	}

	
	
	

}
