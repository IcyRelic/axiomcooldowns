package me.icyrelic.com.Commands;

import me.icyrelic.com.AxiomCooldowns;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class axiomcooldowns implements CommandExecutor {
	
	AxiomCooldowns plugin;
	public axiomcooldowns(AxiomCooldowns instance){
		plugin = instance;
	}
	
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("axiomcooldowns")){
			if(args.length == 1){
				
				if(args[0].equalsIgnoreCase("help")){
					sender.sendMessage(ChatColor.GOLD + "AxiomCooldowns Help");
					sender.sendMessage(ChatColor.GRAY + "/axiomcooldowns help - Displays this");
					sender.sendMessage(ChatColor.GRAY + "/axiomcooldowns reload - Reload the config");
				}else if(args[0].equalsIgnoreCase("reload")){
					if(sender.hasPermission("axiomcooldowns.reload")){
						plugin.reloadConfig();
						sender.sendMessage(plugin.prefix + ChatColor.GREEN + "Config Reloaded.");
					}else{
						sender.sendMessage(plugin.prefix + ChatColor.RED + "You dont have permission");
					}
					
				}else{
					sender.sendMessage(plugin.prefix + ChatColor.RED + "Unknown Command Type /axiomcooldowns help for info.");
				}
				
				
			}else{
				String name = plugin.getDescription().getName();
				String version = " v"+plugin.getDescription().getVersion();
				
				sender.sendMessage(name + version + " by IcyRelic");
			}
		}
		
		return true;
		
		
	}

}
