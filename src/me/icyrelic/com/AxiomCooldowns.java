package me.icyrelic.com;

import java.util.HashMap;
import java.util.UUID;

import me.icyrelic.com.Commands.axiomcooldowns;
import me.icyrelic.com.Console.ConsoleMessage;
import me.icyrelic.com.Listeners.cmd;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public class AxiomCooldowns extends JavaPlugin {
	
	public String prefix = (ChatColor.WHITE+"[" + ChatColor.GOLD + "AxiomCooldowns" + ChatColor.WHITE + "] ");
	public ConsoleMessage console = new ConsoleMessage(prefix);
	public HashMap<UUID, Long> cooldowns = new HashMap<UUID, Long>();
	
	public void onEnable(){
		loadConfiguration();
		loadCommands();
		loadListeners();
		console.sendMessage(ChatColor.WHITE + "Enabled");
		
	}
	
	private void loadListeners(){
		Bukkit.getServer().getPluginManager().registerEvents(new cmd(this), this);
	}
	
	private void loadCommands(){
		getCommand("axiomcooldowns").setExecutor(new axiomcooldowns(this));
	}
	
	private void loadConfiguration(){
	    saveDefaultConfig();	
	}

}
